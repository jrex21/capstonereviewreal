<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>My AngularJS App</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="/capstone-review/jsapp/bower_components/bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" href="/capstone-review/css/app.css">  
</head>
<body ng-app="exampleApp">
<div ng-view></div>

  <script src="/capstone-review/jsapp/bower_components/jquery/jquery.js"></script>
  <script src="/capstone-review/jsapp/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>  
  <script src="/capstone-review/jsapp/bower_components/angular/angular.js"></script>
  <script src="/capstone-review/jsapp/bower_components/angular-route/angular-route.min.js"></script>
  <script src="/capstone-review/jsapp/bower_components/angular-resource/angular-resource.min.js"></script>
  <script src="/capstone-review/jsapp/example/exampleApp.js"></script>
  <script src="/capstone-review/jsapp/example/controllers/exampleCtrl.js"></script>
  <script src="/capstone-review/jsapp/example/services/exPlayerService.js"></script>

</body>
</html>